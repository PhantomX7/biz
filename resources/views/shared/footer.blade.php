<footer class="bg-white border-t border-gray-400 shadow">
    <div class="container max-w-4xl mx-auto flex py-8">

        <div class="w-full mx-auto flex flex-wrap">
            <div class="flex w-full md:w-1/2 ">
                <div class="px-8">
                    <h3 class="font-bold text-gray-900">About</h3>
                    <p class="py-4 text-gray-600 text-sm">
                        Slot online terpercaya yang dapat anda mainkan dan coba sekarang juga. Tersedia berbagai jenis slot online terpercaya dan terbaru.
                    </p>
                </div>
            </div>

            <div class="flex w-full md:w-1/2">
                <div class="px-8">
                    <h3 class="font-bold text-gray-900">Daftar halaman</h3>
                    <ul class="list-reset items-center text-sm pt-3">
                        <li>
                            <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1"
                                href="{{ route('amp', 'pertanyaan-tentang-slot-online')}}">Berbagai pertanyaan mengenai slot online terbaru</a>
                        </li>
                        <li>
                            <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1"
                                href="{{ route('amp', 'daftar-slot-online-terbaru-2021')}}">Daftar slot online terbaru 2021</a>
                        </li>
                        <li>
                            <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1"
                                href="{{ route('web', 'slot-online-terpercaya')}}">Slot online terpercaya</a>
                        </li>
                        <li>
                            <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1"
                                href="{{ route('web', '10-slot-online-terbaru')}}">10 Slot online terpercaya dan terbaru</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
