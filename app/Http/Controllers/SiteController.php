<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;

class SiteController extends Controller
{
    public function postDetails($slug){
        $slugMap = [
            "pertanyaan-tentang-slot-online" => "faq",
            "daftar-slot-online-terbaru-2021" => "1",
            "slot-online-terpercaya" => "2",
            "10-slot-online-terbaru" => "3",
        ];

        if (array_key_exists($slug, $slugMap)) {
            $slug = $slugMap[$slug];
        }
        
        if (request()->segment(1) == 'amp') {
            if (view()->exists('amp.'.$slug)) {
                $view = 'amp.'.$slug;
            } else {
                $view = 'amp.index';
            }
        } else {
            if (view()->exists('main.'.$slug)) {
                $view = 'main.'.$slug;
            } else {
                 $view = 'main.index';
            }
        }

        return view($view);
    }
}